package com.example.android.miwok;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;


public class WordAdapter extends ArrayAdapter<Word> {

    private int mBackgroundColor;


    public WordAdapter(Context context, ArrayList<Word> words, int backgroundColor) {
        super(context,0,words);
        this.mBackgroundColor = backgroundColor;
    }

    /*@Override
    public int getCount() {
        return arrWords.size();
    }

    @Override
    public Word getItem(int position) {
        return arrWords.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
*/
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Check if the existing view is being reused, otherwise inflate the view
        View listItemView = convertView;
        if(listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item, parent, false);
            ((LinearLayout)listItemView.findViewById(R.id.container)).setBackgroundResource(mBackgroundColor);
        }

        // Get the {@link Word} object located at this position in the list
        Word word =  getItem(position);

        // Find the TextView in the list_item.xml layout with the ID version_name
        TextView miwokTextView = (TextView) listItemView.findViewById(R.id.miwok_text_view);
        // Get the miwok word from the current Word object and
        // set this text on the list_item TextView
        miwokTextView.setText(word.getWimokWord());

        // Find the TextView in the list_item.xml layout with the ID version_number
        TextView translationTextView = (TextView) listItemView.findViewById(R.id.english_text_view);
        // Get the translation word from the current Word object and
        // set this text on the list_item TextView
        translationTextView.setText(word.getDefaultWordTranslation());

        // Find the ImageView in the list_item.xml layout with the ID version_number
        ImageView imageView = (ImageView) listItemView.findViewById(R.id.miwok_image_view);
        // Get the getImageResourceId word from the current Word object and
        // set this id on the list_item TextView

        if(word.hasImage()) {
            imageView.setImageResource(word.getImageResourceId());
            imageView.setVisibility(View.VISIBLE);
        }else {
            imageView.setVisibility(View.GONE);
        }

        // Return the whole list item layout (containing 2 TextViews and an ImageView)
        // so that it can be shown in the ListView
        return listItemView;
    }
}
