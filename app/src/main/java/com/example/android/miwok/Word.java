package com.example.android.miwok;

public class Word {

    private static final int NO_IMAGE_PROVIDER = -1;

    private int mAudioResourceId;
    private String mMiwokTranslation;
    private String mDefaultTranslation;
    private int mImageResourceId =  NO_IMAGE_PROVIDER;





    public Word(String wimokWord, String defaultWordTranslation, int soundId) {
        this.mMiwokTranslation = wimokWord;
        this.mDefaultTranslation = defaultWordTranslation;
        this.mAudioResourceId = soundId;
    }

    public Word(String wimokWord, String defaultWordTranslation, int image,  int soundId) {
        this(wimokWord,defaultWordTranslation,soundId);
        this.mImageResourceId = image;

    }

    public String getWimokWord() {
        return mMiwokTranslation;
    }

    public void setWimokWord(String mWimokWord) {
        this.mMiwokTranslation = mWimokWord;
    }

    public String getDefaultWordTranslation() {
        return mDefaultTranslation;
    }

    public void setDefaultWordTranslation(String mDefaultWordTranslation) {
        this.mDefaultTranslation = mDefaultWordTranslation;
    }

    public int getImageResourceId() {
        return mImageResourceId;
    }

    public boolean hasImage(){
        return mImageResourceId != NO_IMAGE_PROVIDER;
    }

    public int getAudiResourceId() {
        return mAudioResourceId;
    }


    /**
     * Returns the string representation of the {@link Word} object.
     */
    @Override
    public String toString() {
        return "Word{" +
                "mDefaultTranslation='" + mDefaultTranslation + '\'' +
                ", mMiwokTranslation='" + mMiwokTranslation + '\'' +
                ", mAudioResourceId=" + mAudioResourceId +
                ", mImageResourceId=" + mImageResourceId +
                '}';
    }
}
